package Tests;

import org.testng.annotations.Test;

import pageObjects.LoginPageObjects;
import testBase.TestBase;


@Test
public class UserLoginTests extends TestBase
{
	
	
	LoginPageObjects loginPage = new LoginPageObjects();
	

	public void ManagerLoginTest() throws Throwable 
	{
		loginPage.login("administrator@localhost.com", "administrator");
		Thread.sleep(2000);
	}
	
	
	public void ClientLoginTest() throws Throwable
	{
		loginPage.login("client@localhost.com", "client");
		Thread.sleep(2000);
	}
	
	
	public void DesignerLoginTest() throws Throwable 
	{
		loginPage.login("developer@localhost.com", "developer");
		Thread.sleep(2000);
		assertEqualsString_custom("ExpectedTest", "ActualText", "LoginPageHomePage");

	}

}
